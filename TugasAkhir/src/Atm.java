import java.util.Scanner;


public class Atm {
      public static void main(String[] args)  {
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("Masukan Saldo Anda : ");
        int saldo = input.nextInt();
        
        
        while(true) {
        
            int saldoSekarang = saldo;
            
            System.out.print("Menu\n"
                    + "1. Cek Saldo\n"
                    + "2. Tambah Saldo\n"
                    + "3. Tarik Saldo\n"
                    + "4. Keluar\n"
                    + "Pilih [1/2/3/4] : ");
            
            String menu = input.next();
            
            if(menu.equalsIgnoreCase("1") ) {
                    
                    System.out.println("Jumlah Saldo Anda adalah Rp. " 
                    + saldoSekarang);
                    
                    
                
            }else if (menu.equalsIgnoreCase("2") ) {
                
                   
                     System.out.println("Anda akan Melakukan Penambahan saldo Tunai\n"
                            + "Masukan Jumlah Saldo Yang Anda Tambah :  ");

                    int penambahan = input.nextInt();
                    saldo = saldo + penambahan;
                    System.out.println("Saldo awal anda adalah Rp. "+ saldoSekarang + ". Anda melakukan penambahan saldo sebesar Rp."+ penambahan);
          
                    
            }else if (menu.equalsIgnoreCase("3") ) {
                
                
                    System.out.println("Anda akan Melakukan Penarikan Tunai\n"
                            + "Masukan Jumlah Penarikan :  ");
                    
          
                    int penarikan = input.nextInt();
                    
                    if  ( penarikan > saldo ) {
                        System.out.println("Saldo Anda Tidak Cukup");
                  
                    }else {
                        saldo = saldo - penarikan;
                        System.out.println("Saldo awal anda adalah Rp." + saldoSekarang + ". Anda melakukan penarikan sebesar Rp." + penarikan);
                    }
                    
                     
                
            }else if (menu.equalsIgnoreCase("4") ){
                
                    System. exit(0);
                
            }else {
                System.out.println("Maaf, Menu transakasi yang anda pilih tidak ada!. "); 
            }
            
            
            
  
            //Project Akhir Atm Sederhana
            
                                
                } 
                    
                 
              
          
            
                
        }
        
        
        
        
  
        
    }